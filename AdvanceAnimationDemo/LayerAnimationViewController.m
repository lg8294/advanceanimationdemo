//
//  LayerAnimationViewController.m
//  1120_ketanglianxi
//
//  Created by lg on 14/11/20.
//  Copyright (c) 2014年 lg. All rights reserved.
//

#import "LayerAnimationViewController.h"

@interface LayerAnimationViewController ()
{
    UIView *_blockView;
    
    UIImageView * _imageView;
}

@end

@implementation LayerAnimationViewController

- (void)dealloc
{
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setTitle:@"Layer"];
    }
    return self;
}

- (void)initUsetInterface
{
    _blockView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 150, 150)];
    _blockView.backgroundColor = [UIColor whiteColor];
    _blockView.center = self.view.center;
    
    //_blockView.layer.borderWidth = 1;//边界宽度
    //_blockView.layer.borderColor = [UIColor redColor].CGColor;//边界颜色
    _blockView.layer.cornerRadius = _blockView.bounds.size.height/2;//圆角
    
    _blockView.layer.shadowOffset = CGSizeMake(0, 0);//阴影范围
    _blockView.layer.shadowColor = [UIColor redColor].CGColor;//阴影颜色
    _blockView.layer.shadowRadius = 30;
    _blockView.layer.shadowOpacity = 0.7;//阴影透明度
    
    [self.view addSubview:_blockView];
    
    _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 150, 150)];
    _imageView.backgroundColor = [UIColor whiteColor];
    [_imageView setImage:[UIImage imageNamed:@"head4"]];
    _imageView.center = self.view.center;

    [_imageView setClipsToBounds:YES];
    //[_imageView.layer setMasksToBounds:YES];
    
    //_imageView.layer.borderWidth = 1;//边界宽度
    //_imageView.layer.borderColor = [UIColor redColor].CGColor;//边界颜色
    _imageView.layer.cornerRadius = _imageView.bounds.size.height/2;//圆角
    
//    _imageView.layer.shadowPath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(-5, -5, _imageView.bounds.size.width + 10, _imageView.bounds.size.height + 10) cornerRadius:_imageView.bounds.size.height/2 + 5].CGPath;
    
    [self.view addSubview:_imageView];
}

- (void)anima
{
    [UIView beginAnimations:@"Color" context:nil];
    [UIView setAnimationDuration:2];
    [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:_blockView cache:NO];
    _blockView.backgroundColor = [UIColor redColor];
    [UIView commitAnimations];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self anima];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initUsetInterface];
}


@end






























