//
//  CATransitionViewController.m
//  1120_ketanglianxi
//
//  Created by lg on 14/11/20.
//  Copyright (c) 2014年 lg. All rights reserved.
//

#import "CATransitionViewController.h"

@interface CATransitionViewController () <CAAnimationDelegate>
{
    UILabel *_titleLabel;

}

@end

@implementation CATransitionViewController
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setTitle:@"Transition"];
    }
    return self;
}
- (void)initUserInterface
{
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 200)];
    _titleLabel.font = [UIFont systemFontOfSize:30];
    _titleLabel.text = @"Transition";
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:_titleLabel];
}

- (void)anima
{
    CATransition *transition = [[CATransition alloc] init];
    [transition setDelegate:self];
    [transition setDuration:0.5];//设置时长
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];//设置变化规律
    
    
    //---------------------------CAAnimation----------------------------//
    
    //    [transition setAutoreverses:YES];//设置完成后是否逆向执行动画
    //    [transition setBeginTime:3];//设置延迟
    //
    //    [transition setFillMode:kCAFillModeForwards];//设置动画结束后的状态
    //    [transition setRemovedOnCompletion:NO];//动画结束后删除动画，若为YES，则fillMode无效（与上一句代码配合使用）
    //
    //    [transition setRepeatCount:3];//设置重复次数
    
    //--------------------------Transition-----------------------------//
    
    //    [transition setType:kCATransitionPush];//设置过度样式
    [transition setType:@"cameraIrisHollowOpen"];
    [transition setSubtype:kCATransitionFromTop];//子样式
    
    //suckEffect 抽纸效果
    //cube 方块效果
    //oglFlip 翻转效果 rippleEffect 水波纹效果 cameraIrisHollowOpen 快门效果 pageCurl 翻页效果
    //添加到layer层就开始执行动画（设置了延迟除外）
    [self.view.layer addAnimation:transition forKey:@"transition"];
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self anima];
}

#pragma mark - CAAnimationDelegate
- (void)animationDidStart:(CAAnimation *)anim {
    NSLog(@"%s", __func__);
}
- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    self.view.backgroundColor = [UIColor brownColor];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initUserInterface];
}

@end























