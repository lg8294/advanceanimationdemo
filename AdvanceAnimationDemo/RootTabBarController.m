//
//  RootTabBarController.m
//  1120_ketanglianxi
//
//  Created by lg on 14/11/20.
//  Copyright (c) 2014年 lg. All rights reserved.
//

#import "RootTabBarController.h"
#import "LayerAnimationViewController.h"
#import "CABasicAnimationViewController.h"
#import "CAKeyFrameViewController.h"
#import "CAGroupViewController.h"
#import "CATransitionViewController.h"

@interface RootTabBarController ()

@end

@implementation RootTabBarController

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        LayerAnimationViewController * layerVC = [[LayerAnimationViewController alloc] init];
        
        //转场动画
        CATransitionViewController * transitonVC = [[CATransitionViewController alloc] init];
        
        //基础动画
        CABasicAnimationViewController * basicVC = [[CABasicAnimationViewController alloc] init];
        
        //关键帧动
        CAKeyFrameViewController * frameVC = [[CAKeyFrameViewController alloc] init];
        
        //动画组
        CAGroupViewController * groupVC = [[CAGroupViewController alloc] init];
        
        [self setViewControllers:@[layerVC, transitonVC, basicVC, frameVC, groupVC]];

    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


@end
