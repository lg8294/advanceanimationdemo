//
//  AppDelegate.h
//  AdvanceAnimationDemo
//
//  Created by lg on 15/10/27.
//  Copyright © 2015年 lg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

