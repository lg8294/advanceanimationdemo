//
//  CABasicAnimationViewController.m
//  1120_ketanglianxi
//
//  Created by lg on 14/11/20.
//  Copyright (c) 2014年 lg. All rights reserved.
//

#import "CABasicAnimationViewController.h"

@interface CABasicAnimationViewController ()
{
    UILabel *_titleLabel;
}
@end

@implementation CABasicAnimationViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setTitle:@"Basic"];
    }
    return self;
}
- (void)initUserInterface
{
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(00, CGRectGetMidY(self.view.bounds), CGRectGetWidth(self.view.bounds), 100)];
    _titleLabel.font = [UIFont systemFontOfSize:50];
    _titleLabel.text = @"Basic";
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    _titleLabel.backgroundColor = [UIColor redColor];
    [self.view addSubview:_titleLabel];
}

- (void)anima
{
    //-----------------------------BasicAnimation-------------------------//
    
    CABasicAnimation *basicAnmation = [CABasicAnimation animationWithKeyPath:@"position.y"];
    //position 位置 opacity 透明度 transform.rotation.z 转动
    [basicAnmation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [basicAnmation setDuration:2];
    
    //    [basicAnmation setToValue:[NSValue valueWithCGPoint:CGPointMake(100, 400)]];//设置属性值的改变
    //    [basicAnmation setToValue:@0.5];
//    [basicAnmation setToValue:@(M_PI)];
    [basicAnmation setToValue:@(CGRectGetMidY(self.view.bounds))];
    //    [basicAnmation setFillMode:kCAFillModeForwards];
    //    [basicAnmation setRemovedOnCompletion:NO];
    [basicAnmation setAutoreverses:YES];
    [basicAnmation setRepeatCount:3];
    
    [_titleLabel.layer addAnimation:basicAnmation forKey:@"position"];
}



- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self anima];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self initUserInterface];
}


@end
