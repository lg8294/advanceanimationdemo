//
//  CAKeyFrameViewController.m
//  1120_ketanglianxi
//
//  Created by lg on 14/11/20.
//  Copyright (c) 2014年 lg. All rights reserved.
//

#import "CAKeyFrameViewController.h"

@interface CAKeyFrameViewController ()
{
    UILabel *_titleLabel;
}
@end

@implementation CAKeyFrameViewController
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setTitle:@"Frame"];
    }
    return self;
}
- (void)initUserInterface
{
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,0,150,50)];
    _titleLabel.center = self.view.center;
    
    _titleLabel.font = [UIFont systemFontOfSize:50];
    _titleLabel.text = @"Frame";
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    _titleLabel.backgroundColor = [UIColor redColor];
    
    [self.view addSubview:_titleLabel];
}

- (void)anima
{
    //----------------------------CAKeyFrameAnimation-------------------------------//
    CAKeyframeAnimation * keyFrameAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    [keyFrameAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    
    [keyFrameAnimation setCalculationMode:kCAAnimationCubicPaced];//
    [keyFrameAnimation setKeyTimes:@[@1,@2,@3,@4]];
    [keyFrameAnimation setValues:@[[NSValue valueWithCGPoint:CGPointMake(10, 20)],
                                   [NSValue valueWithCGPoint:CGPointMake(50, 70)],
                                   [NSValue valueWithCGPoint:CGPointMake(100, 20)],
                                   [NSValue valueWithCGPoint:CGPointMake(43, 200)],]];
    
    //--------------------------UIBezierPath---------------------------------
//    UIBezierPath *path = [[UIBezierPath alloc] init];
//    [path moveToPoint:self.view.center];
//    [path addLineToPoint:CGPointMake(270, 360)];
//    [path addLineToPoint:CGPointMake(270, 80)];
//    //addLine...
//    [path closePath];
//    
//    [keyFrameAnimation setPath:path.CGPath];
    
    
    
    
    [_titleLabel.layer addAnimation:keyFrameAnimation forKey:@"position"];
    
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self anima];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self initUserInterface];
}


@end
