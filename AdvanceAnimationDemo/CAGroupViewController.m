//
//  CAGroupViewController.m
//  1120_ketanglianxi
//
//  Created by lg on 14/11/20.
//  Copyright (c) 2014年 lg. All rights reserved.
//

#import "CAGroupViewController.h"

@interface CAGroupViewController () <CAAnimationDelegate>
{
    UILabel *_titleLabel;
}
@end

@implementation CAGroupViewController
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setTitle:@"Group"];
    }
    return self;
}
- (void)initUserInterface
{
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 200)];
    _titleLabel.font = [UIFont systemFontOfSize:30];
    _titleLabel.text = @"Group";
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:_titleLabel];
}

- (void)anima
{
    CATransition *transition = [[CATransition alloc] init];
    [transition setDelegate:self];
    [transition setDuration:0.5];//设置时长
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];//设置变化规律
    //---------------------------CAAnimation----------------------------//
    //    [transition setAutoreverses:YES];//设置完成后是否逆向执行动画
    //    [transition setBeginTime:3];//设置延迟
    //
    //    [transition setFillMode:kCAFillModeForwards];//设置动画结束后的状态
    //    [transition setRemovedOnCompletion:NO];//动画结束后删除动画，若为YES，则fillMode无效（与上一句代码配合使用）
    //
    //    [transition setRepeatCount:3];//设置重复次数
    //--------------------------Transition-----------------------------//
    //    [transition setType:kCATransitionPush];//设置过度样式
    [transition setType:@"cameraIrisHollowOpen"];
    [transition setSubtype:kCATransitionFromTop];//子样式
    //suckEffect 抽纸效果
    //cube 方块效果
    //oglFlip 翻转效果 rippleEffect 水波纹效果 cameraIrisHollowOpen 快门效果 pageCurl 翻页效果 pageunCurl
    //添加到layer层就开始执行动画（设置了延迟除外）
    
    
    //--------------------------------改变颜色---------------------------------//
    //_titleLabel.layer setBackgroundColor:<#(CGColorRef)#>
    CABasicAnimation *backgroundColorAnmation = [CABasicAnimation animationWithKeyPath:@"backgroundColor"];
    [backgroundColorAnmation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [backgroundColorAnmation setDuration:4];
    [backgroundColorAnmation setToValue:(id)[UIColor redColor].CGColor];//<#(CGColorRef)#>
    [backgroundColorAnmation setFillMode:kCAFillModeForwards];
    [backgroundColorAnmation setRemovedOnCompletion:NO];
    
    //----------------------------CAKeyFrameAnimation 改变位置-------------------------------//
    CAKeyframeAnimation * keyFrameAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    [keyFrameAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    
    [keyFrameAnimation setCalculationMode:kCAAnimationCubicPaced];//
    [keyFrameAnimation setKeyTimes:@[@1,@2,@3,@4]];
    [keyFrameAnimation setValues:@[[NSValue valueWithCGPoint:CGPointMake(50, 100)],
                                   [NSValue valueWithCGPoint:CGPointMake(250, 100)],
                                   [NSValue valueWithCGPoint:CGPointMake(50, 500)],
                                   [NSValue valueWithCGPoint:CGPointMake(250, 500)],]];
    
    //--------------------------UIBezierPath---------------------------------
    //    UIBezierPath *path = [[UIBezierPath alloc] init];
    //    [path moveToPoint:self.view.center];
    //    [path addLineToPoint:CGPointMake(270, 360)];
    //    [path addLineToPoint:CGPointMake(270, 80)];
    //    //addLine...
    //    [path closePath];
    //
    //    [keyFrameAnimation setPath:path.CGPath];

    
    CAAnimationGroup * animationGroup = [[CAAnimationGroup alloc] init];
    [animationGroup setDuration:4];
    [animationGroup setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    
    [animationGroup setAnimations:@[backgroundColorAnmation,keyFrameAnimation]];
    
    [_titleLabel.layer addAnimation:animationGroup forKey:@"GroupAnimation"];
}

- (void)teacherAnima
{
    //位置
    CABasicAnimation *positionAnimation = [CABasicAnimation animationWithKeyPath:@"position"];
    [positionAnimation setDuration:0.5];
    [positionAnimation setBeginTime:0];
    [positionAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [positionAnimation setFromValue:[NSValue valueWithCGPoint:self.view.center]];
    [positionAnimation setToValue:[NSValue valueWithCGPoint:CGPointMake(CGRectGetMidX(self.view.bounds), 100)]];
    [positionAnimation setRemovedOnCompletion:NO];
    
    //透明度
    CABasicAnimation *opacityAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    [opacityAnimation setDuration:0.5];
    [opacityAnimation setBeginTime:0.5];
    [opacityAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [opacityAnimation setFromValue:@1];
    [opacityAnimation setToValue:@0.5];
    [opacityAnimation setRemovedOnCompletion:NO];
    
    //旋转
    CABasicAnimation *rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    [rotationAnimation setDuration:0.5];
    [rotationAnimation setBeginTime:1];
    [rotationAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [rotationAnimation setFromValue:@0];
    [rotationAnimation setToValue:@(M_PI)];
    [rotationAnimation setRemovedOnCompletion:NO];
    
    //颜色
    CABasicAnimation * colorAnimation = [CABasicAnimation animationWithKeyPath:@"backgroundColor"];
    [colorAnimation setDuration:1.2];
    [colorAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [colorAnimation setFromValue:((id)[UIColor greenColor].CGColor)];
    [colorAnimation setToValue:((id)[UIColor redColor].CGColor)];
    [colorAnimation setFillMode:kCAFillModeForwards];
    [colorAnimation setRemovedOnCompletion:NO];
    
    
    
    //动画组
    CAAnimationGroup *animationGroup = [CAAnimationGroup animation];
    [animationGroup setDuration:1.5]; //动画组时长优先级最高
    [animationGroup setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [animationGroup setAnimations:@[positionAnimation, opacityAnimation, rotationAnimation,colorAnimation]];
    [animationGroup setRemovedOnCompletion:NO];
    [animationGroup setFillMode:kCAFillModeBackwards];
    [_titleLabel.layer addAnimation:animationGroup forKey:@"GroupAnimation"];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
//    [self teacherAnima];
    [self anima];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initUserInterface];
}

#pragma mark - CAAnimationDelegate
- (void)animationDidStart:(CAAnimation *)anim {
    NSLog(@"%s", __func__);
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag {
    NSLog(@"%s", __func__);
}

@end
